package com.example.demo;

import biztalk.ksrzis.ness.CudImportujData;
import biztalk.ksrzis.ness.ImportujData;
import cz.ksrzis.ereg.ImportXMLRequest;
import cz.ksrzis.ereg.ImportXMLResponse;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.security.auth.callback.CallbackHandler;
import javax.xml.ws.BindingProvider;
import java.io.File;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
    private static Logger logger = LoggerFactory.getLogger(DemoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    private ImportujData createImportujData(File pkcs12File, String keyName, String keyPassword, String endpointUrl) {
        CudImportujData cid = new CudImportujData();
        ImportujData binding = cid.getBasicHttpBindingCudImport();

        Properties tlsProperties = new Properties();
        tlsProperties.put("org.apache.ws.security.crypto.provider", "org.apache.ws.security.components.crypto.Merlin");
        tlsProperties.put("org.apache.ws.security.crypto.merlin.keystore.file", pkcs12File.toURI().toString());
        tlsProperties.put("org.apache.ws.security.crypto.merlin.keystore.password", keyPassword);
        tlsProperties.put("org.apache.ws.security.crypto.merlin.keystore.type", "pkcs12");
        tlsProperties.put("org.apache.ws.security.crypto.merlin.keystore.alias", keyName);

        Map<String, Object> requestContext = ((BindingProvider) binding).getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
        requestContext.put("ws-security.signature.properties", tlsProperties);
        requestContext.put("ws-security.encryption.properties", tlsProperties);
        requestContext.put("ws-security.signature.username", keyName);
        requestContext.put("ws-security.encryption.username", keyName);

        requestContext.put("ws-security.callback-handler", (CallbackHandler) callbacks ->
                Arrays.stream(callbacks).forEach(cb -> ((WSPasswordCallback)cb).setPassword(keyPassword)));

        return binding;
    }

    @Override
    public void run(String... args) {
        if (args.length != 1) {
            logger.error("Specify password on command line");
            System.exit(1);
        }

        ImportujData binding = createImportujData(
                new File("./rgu_ws_00064211_TEST.pfx"),
                "le-systemaccount-ereg-557bb5ba-c45a-45f5-8f3f-fc61d67dd976",
                args[0],
                "https://eregws2.ksrzis.cz/Registr/CUD/Import");

        ImportXMLRequest request = new ImportXMLRequest();
        request.setTypFormulare("ABCD");
        request.setVerzeFormulare(BigInteger.ONE);
        request.setObsah(new byte[] { 1, 2, 3 });

        logger.info("Requesting ...");

        try {
            ImportXMLResponse response = binding.importXML(request);
            logger.info("Response: {}", new ReflectionToStringBuilder(response));
        } catch (Exception e) {
            logger.error("Error calling: ", e);
        }
    }
}
